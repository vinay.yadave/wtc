package com.buralo.wtc.api.cassandra.domain;

import com.buralo.wtc.api.domain.Message;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("messages")
public class CassandraMessage implements Message {

    private String id;

    @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String room;

    private String sender;

    @Column("message_language")
    private String messageLanguage;

    @Column("message_text")
    private String messageText;

    @PrimaryKeyColumn(name = "message_timestamp", ordinal = 1)
    private long messageTimestamp;

    public CassandraMessage() {
    }

    public CassandraMessage(final String id,
                            final String room,
                            final String sender,
                            final String messageLanguage,
                            final String messageText,
                            final long messageTimestamp) {
        this.id = id;
        this.room = room;
        this.sender = sender;
        this.messageLanguage = messageLanguage;
        this.messageText = messageText;
        this.messageTimestamp = messageTimestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getRoom() {
        return room;
    }

    public void setRoom(final String room) {
        this.room = room;
    }

    @Override
    public String getSender() {
        return sender;
    }

    public void setSender(final String sender) {
        this.sender = sender;
    }

    @Override
    public String getMessageLanguage() {
        return messageLanguage;
    }

    public void setMessageLanguage(final String messageLanguage) {
        this.messageLanguage = messageLanguage;
    }

    @Override
    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(final String messageText) {
        this.messageText = messageText;
    }

    @Override
    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    public void setMessageTimestamp(final long messageTimestamp) {
        this.messageTimestamp = messageTimestamp;
    }
}
