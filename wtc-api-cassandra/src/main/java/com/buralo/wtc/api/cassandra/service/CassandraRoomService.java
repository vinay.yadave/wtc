/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.cassandra.service;

import com.buralo.wtc.api.cassandra.domain.CassandraRoom;
import com.buralo.wtc.api.cassandra.repository.RoomRepository;
import com.buralo.wtc.api.domain.Room;
import com.buralo.wtc.api.service.database.AbstractRoomService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CassandraRoomService extends AbstractRoomService {

    private final RoomRepository repository;

    CassandraRoomService(final IdentifierService identifierService,
                         final RoomRepository repository) {
        super(identifierService);
        this.repository = repository;
    }

    @Override
    public Flux<Room> fetchRooms() {
        return repository.findAll().cast(Room.class);
    }

    @Override
    public Mono<Room> createRoom(final String name,
                                 final boolean secret) {
        return repository
                .existsByName(name)
                .filter(Boolean::booleanValue)
                .flatMap(exists -> reportDuplicateRoom(name))
                .switchIfEmpty(doCreateRoom(name, secret));
    }

    private Mono<Room> reportDuplicateRoom(final String name) {
        return Mono.defer(() -> Mono.error(new IllegalStateException("Room " + name + " already exists")));
    }

    private Mono<Room> doCreateRoom(final String name,
                                    final boolean secret) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new CassandraRoom(id, name, secret))
                .flatMap(repository::insert)
                .cast(Room.class);
    }

    @Override
    public Mono<Room> getRoom(final String id) {
        return repository
                .findById(id)
                .cast(Room.class);
    }

    @Override
    public Mono<Void> updateRoom(final String id,
                                 final String name,
                                 final boolean secret) {
        return repository
                .findById(id)
                .map(room -> new CassandraRoom(id, name, secret))
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteRoom(final String id) {
        return repository
                .deleteById(id);
    }
}
