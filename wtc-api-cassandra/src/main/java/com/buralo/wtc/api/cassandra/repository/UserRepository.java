package com.buralo.wtc.api.cassandra.repository;

import com.buralo.wtc.api.cassandra.domain.CassandraUser;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveCassandraRepository<CassandraUser, String> {

    Mono<Boolean> existsByNickname(String nickname);

    Mono<Boolean> existsByEmailAddress(String emailAddress);
}
