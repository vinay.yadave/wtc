package com.buralo.wtc.api.cassandra.domain;

import com.buralo.wtc.api.domain.User;
import com.buralo.wtc.api.domain.UserFactory;
import org.springframework.stereotype.Component;

@Component
public class CassandraUserFactory implements UserFactory {
    @Override
    public User createUser(final String id,
                           final String name,
                           final String nickname,
                           final String emailAddress,
                           final String token,
                           final String password) {
        return new CassandraUser(id, name, nickname, emailAddress, token, password);
    }
}
