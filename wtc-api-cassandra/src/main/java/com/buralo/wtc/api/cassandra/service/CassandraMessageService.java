/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.cassandra.service;

import com.buralo.wtc.api.cassandra.domain.CassandraMessage;
import com.buralo.wtc.api.cassandra.repository.MessageRepository;
import com.buralo.wtc.api.domain.Message;
import com.buralo.wtc.api.service.database.AbstractMessageService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class CassandraMessageService extends AbstractMessageService {

    private final MessageRepository repository;

    CassandraMessageService(final IdentifierService identifierService,
                            final Clock clock,
                            final MessageRepository repository) {
        super(identifierService, clock);
        this.repository = repository;
    }

    @Override
    public Flux<Message> streamMessages(final String room,
                                        final String marker) {
        return repository
                .findMessagesByRoomAndMessageTimestampGreaterThan(room, Long.valueOf(marker))
                .cast(Message.class);
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return repository
                .findMessagesByRoomAndMessageTimestampGreaterThan(room, Long.valueOf(marker))
                .cast(Message.class);
    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService().generateIdentifier()
                .map(id -> new CassandraMessage(
                        id,
                        room,
                        sender,
                        language,
                        text,
                        getClock().millis()))
                .flatMap(repository::insert)
                .cast(Message.class);
    }
}
