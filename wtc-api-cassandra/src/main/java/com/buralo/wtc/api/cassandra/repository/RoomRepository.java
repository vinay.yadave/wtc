package com.buralo.wtc.api.cassandra.repository;

import com.buralo.wtc.api.cassandra.domain.CassandraRoom;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Mono;

public interface RoomRepository extends ReactiveCassandraRepository<CassandraRoom, String> {

    Mono<Boolean> existsByName(String name);
}
