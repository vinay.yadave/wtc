package com.buralo.wtc.api.cassandra.domain;

import com.buralo.wtc.api.domain.Room;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("rooms")
public class CassandraRoom implements Room {

    @PrimaryKey
    private String id;

    @Indexed("rooms_name_idx")
    private String name;

    private boolean secret;

    public CassandraRoom() {
    }

    public CassandraRoom(final String id,
                         final String name,
                         final boolean secret) {
        this.id = id;
        this.name = name;
        this.secret = secret;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean isSecret() {
        return secret;
    }

    public void setSecret(final boolean secret) {
        this.secret = secret;
    }
}
