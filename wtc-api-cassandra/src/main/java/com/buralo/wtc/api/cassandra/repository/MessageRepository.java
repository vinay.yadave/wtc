package com.buralo.wtc.api.cassandra.repository;

import com.buralo.wtc.api.cassandra.domain.CassandraMessage;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;

public interface MessageRepository extends ReactiveCassandraRepository<CassandraMessage, String> {

    Flux<CassandraMessage> findMessagesByRoomAndMessageTimestampGreaterThan(String room, long message_timestamp);
}
