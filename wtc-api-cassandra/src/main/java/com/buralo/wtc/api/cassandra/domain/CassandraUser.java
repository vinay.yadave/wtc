package com.buralo.wtc.api.cassandra.domain;

import com.buralo.wtc.api.domain.User;
import jnr.ffi.annotations.In;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("users")
public class CassandraUser implements User {

    @PrimaryKey
    private String id;

    private String name;

    @Indexed("users_nickname_idx")
    private String nickname;

    @Column("email_address")
    @Indexed("users_email_idx")
    private String emailAddress;

    @Column("password_token")
    private String token;

    private String password;

    public CassandraUser() {
    }

    public CassandraUser(final String id,
                         final String name,
                         final String nickname,
                         final String emailAddress,
                         final String token,
                         final String password) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.emailAddress = emailAddress;
        this.token = token;
        this.password = password;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    public void setNickname(final String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
