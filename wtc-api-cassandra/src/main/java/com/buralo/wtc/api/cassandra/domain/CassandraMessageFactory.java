package com.buralo.wtc.api.cassandra.domain;

import com.buralo.wtc.api.domain.Message;
import org.springframework.stereotype.Component;

@Component
public class CassandraMessageFactory implements com.buralo.wtc.api.domain.MessageFactory {
    @Override
    public Message createMessage(final String id,
                                 final String room,
                                 final String sender,
                                 final String messageLanguage,
                                 final String messageText,
                                 final long messageTimestamp) {
        return new CassandraMessage(id, room, sender, messageLanguage, messageText, messageTimestamp);
    }
}
