package com.buralo.bot.qotd.config;

import io.rsocket.RSocket;
import io.rsocket.RSocketFactory;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.transport.netty.client.TcpClientTransport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.util.MimeTypeUtils;

@Configuration
public class BotConfig {

    @Bean
    public RSocketRequester requester(final RSocketStrategies rSocketStrategies) {
        return RSocketRequester
                .create(this.rSocket(), MimeTypeUtils.APPLICATION_JSON, rSocketStrategies);
    }

    @Bean
    public RSocket rSocket() {
        return RSocketFactory
                .connect()
                .frameDecoder(PayloadDecoder.ZERO_COPY)
                .dataMimeType(MimeTypeUtils.APPLICATION_JSON_VALUE)
                .transport(TcpClientTransport.create(9898))
                .start()
                .block();
    }
}
