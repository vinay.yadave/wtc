```sql
CREATE USER wtcapi PASSWORD 'wtcapi';
CREATE USER wtcdbo PASSWORD 'wtcdbo';
CREATE DATABASE wtc WITH OWNER = wtcdbo;
```

```sql
CREATE SCHEMA wtc;
CREATE TABLE wtc.messages (
    id VARCHAR(36) NOT NULL,
    room VARCHAR(36) NOT NULL,
    sender VARCHAR(36) NOT NULL,
    message_language VARCHAR(2) NOT NULL,
    message_text TEXT NOT NULL,
    message_timestamp BIGINT NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE wtc.rooms (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(36) NOT NULL,
    secret BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE wtc.occupants (
    room VARCHAR(36) NOT NULL,
    user VARCHAR(36) NOT NULL,
    role VARCHAR(10) NOT NULL,
    PRIMARY KEY (room, user, role)
);
CREATE TABLE wtc.users (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(100) NOT NULL,
    nickname VARCHAR(20) NOT NULL,
    email_address VARCHAR(200) NOT NULL,
    password VARCHAR(100),
    token VACHAR(1000),
    PRIMARY KEY (id)
);
GRANT ALL ON SCHEMA wtc TO wtcapi;
GRANT ALL ON SCHEMA wtc TO wtcdbo;
GRANT ALL ON ALL TABLES IN SCHEMA wtc TO wtcapi;
GRANT ALL ON ALL TABLES IN SCHEMA wtc TO wtcdbo;
```


```sql
insert into wtc.rooms(id, name, secret) values ('ecffc982-66fd-411c-aee9-b0fb87ee8957', 'General', false);
insert into wtc.messages (id, room, sender, message_language, message_text, message_timestamp) values ('ecffc982-66fd-411c-aee9-b0fb87ee8957', 'ecffc982-66fd-411c-aee9-b0fb87ee8957', '645df5fd-0c3a-4471-b46f-e1b7caeda0f2', 'en', 'What''s the craic?', 1547711140998);
insert into wtc.users(id, name, nickname, email_address, password) values ('645df5fd-0c3a-4471-b46f-e1b7caeda0f2', 'Brian Matthews', 'bmatthews68', 'brian@btmatthews.com', 'everclear');

```

```bash
curl http://localhost:8080/rooms/ecffc982-66fd-411c-aee9-b0fb87ee8957/messages
```

```bash
curl -H "Accept-Language: ru" http://localhost:8080/rooms/ecffc982-66fd-411c-aee9-b0fb87ee8957/messages
```

```bash
java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.2/jasypt-1.9.2.jar  org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="wtcapi" password=everclear algorithm=PBEWithMD5AndDES
```



http POST http://localhost:8080/rooms/ecffc982-66fd-411c-aee9-b0fb87ee8957/messages text:='Sucking diesel' language=en


```
java -jar ~/.m2/repository/org/jasypt/jasypt/1.9.2/jasypt-1.9.2.jar
```


https://translate.yandex.com/developers


http POST http://localhost:8080/users name="Brian Matthews" nickname="bmatthews68" emailAddress="brian@btmatthews.com"
http POST http://localhost:8080/rooms name="General" secret=false

    http POST http://localhost:8080/rooms/f114603d-0f40-493f-bd3d-02544664f223/messages Content-Language:en 
  
## Cassandra
  
## Couchbase

For Couchbase we need to create indexes in the bucket.
    
```    
CREATE PRIMARY INDEX ON wtc USING GSI;
CREATE INDEX idx_name ON wtc(name) USING GSI
CREATE INDEX idx_nickname ON wtc(nickname) USING GSI
CREATE INDEX idx_emailAddress ON wtc(emailAddress) USING GSI
```

## MongoDB

## Redis