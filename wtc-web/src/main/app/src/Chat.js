import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default class ChatRoom extends Component {
    render() {
        return (
            <div>
            <h2>{this.props.room.name}</h2>
            <MessagesList baseUrl={this.props.baseUrl} room={this.props.room} />
            <hr />
            <MessageInputForm baseUrl={this.props.baseUrl} room={this.props.room} user={this.props.user} />
            </div>
        );
    }
}

class MessagesList extends Component {
    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1, messages: []};
        this.eventSource = new EventSource(this.props.baseUrl + '/rooms/' + this.props.room.id + '/messages')
    }

    render() {
        return (
            <ul class="messages">
                {this.state.messages.map((message) =>
                    <li class="message">
                        <span class="sender">{message.sender}</span>
                        <span class="timestamp">{message.timestamp}</span>
                        <br />
                        <span class="text">{message.text}</span>
                    </li>
                )}
            </ul>
        );
     }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessages(JSON.parse(e.data));
    }

    addChatMessages(message) {
        console.log(message);
        if (message.timestamp > this.state.last_timestamp) {
            this.setState({last_timestamp: message.timestamp, messages: [...this.state.messages, message]});
        }
    }
}

class MessageInputForm extends Component {
    constructor(props) {
        super(props);
        this.state = { message: '', disabled: true };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <Form>
                <Form.Control value={this.state.message} onChange={this.handleChange} />
                <Button variant="primary" disabled={this.state.disabled} onClick={this.state.disabled ? null : this.handleSubmit}>Send</Button>
            </Form>
        );
    }

    handleChange(e) {
        this.setState({message: e.target.value, disabled: e.target.value == ''});
    }

    handleSubmit(e) {
        e.preventDefault();
        const me = this;
        const message = this.state.message;
        me.setState({message: '', disabled: true})
        fetch(this.props.baseUrl + '/users/' + this.props.user + '/rooms/' + this.props.room.id + '/messages', {
            mode: 'no-cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'text/plain'
            },
            body: message
        }).then(function (response) {
        }).catch(function (error) {
            me.setState({message: message, disabled: false})
        });
    }
}