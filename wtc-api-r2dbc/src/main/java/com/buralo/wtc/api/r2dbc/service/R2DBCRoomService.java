/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.r2dbc.service;

import com.buralo.wtc.api.domain.Room;
import com.buralo.wtc.api.r2dbc.domain.RoomImpl;
import com.buralo.wtc.api.r2dbc.repository.R2DBCRoomRepository;
import com.buralo.wtc.api.service.database.AbstractRoomService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.data.r2dbc.function.DatabaseClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class R2DBCRoomService extends AbstractRoomService {

    private final R2DBCRoomRepository repository;

    private final DatabaseClient databaseClient;

    R2DBCRoomService(final IdentifierService identifierService,
                     final DatabaseClient databaseClient,
                     final R2DBCRoomRepository repository) {
        super(identifierService);
        this.repository = repository;
        this.databaseClient = databaseClient;
    }

    @Override
    public Flux<Room> fetchRooms() {
        return repository.findAll().cast(Room.class);
    }

    @Override
    public Mono<Room> createRoom(final String name,
                                 final boolean secret) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new RoomImpl(id, name, secret))
                .flatMap(room ->
                        databaseClient
                                .insert()
                                .into(RoomImpl.class)
                                .using(room)
                                .fetch()
                                .rowsUpdated()
                                .thenReturn(room))
                .cast(Room.class);
    }

    @Override
    public Mono<Room> getRoom(final String id) {
        return repository.findById(id).cast(Room.class);
    }

    @Override
    public Mono<Void> updateRoom(final String id,
                                 final String name,
                                 final boolean secret) {
        return repository
                .findById(id)
                .map(room -> new RoomImpl(id, name, secret))
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteRoom(final String id) {
        return repository.deleteById(id);
    }
}
