/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.r2dbc.service;

import com.buralo.wtc.api.domain.Message;
import com.buralo.wtc.api.domain.MessageFactory;
import com.buralo.wtc.api.r2dbc.domain.MessageImpl;
import com.buralo.wtc.api.service.database.AbstractMessageService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.data.r2dbc.function.DatabaseClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class R2DBCMessageService extends AbstractMessageService {

    private final DatabaseClient client;

    private  final MessageFactory messageFactory;

    R2DBCMessageService(final DatabaseClient client,
                        final MessageFactory messageFactory,
                        final IdentifierService identifierService,
                        final Clock clock) {
        super(identifierService, clock);
        this.messageFactory = messageFactory;
        this.client = client;
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return client
                .execute()
                .sql("SELECT m.id as id, m.room as room, u.nickname as sender, m.message_language as message_language, m.message_text as message_text, m.message_timestamp as message_timestamp FROM messages as m left outer join users as u ON m.sender = u.id WHERE m.room = $1 AND m.message_timestamp > $2 ORDER BY m.message_timestamp")
                .bind("$1", room)
                .bind("$2", Long.valueOf(marker))
                .as(MessageImpl.class)
                .fetch()
                .all()
                .cast(Message.class);
    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> messageFactory.createMessage(id, room, sender, language, text, getClock().millis()))
                .cast(MessageImpl.class)
                .flatMap(message -> client.insert()
                        .into(MessageImpl.class)
                        .table("messages")
                        .using(message)
                        .fetch()
                        .one()
                        .map(result -> message));
    }
}
