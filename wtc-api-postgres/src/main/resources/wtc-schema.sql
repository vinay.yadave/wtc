CREATE SCHEMA IF NOT EXISTS wtc;

CREATE TABLE IF NOT EXISTS wtc.messages (
    id VARCHAR(36) NOT NULL,
    room VARCHAR(36) NOT NULL,
    sender VARCHAR(36) NOT NULL,
    message_language VARCHAR(2) NOT NULL,
    message_text TEXT NOT NULL,
    message_timestamp BIGINT NOT NULL,
    PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS messages_room_idx ON wtc.messages (room);

CREATE TABLE IF NOT EXISTS wtc.rooms (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(36) NOT NULL,
    secret BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS rooms_name_idx ON wtc.rooms (name);

CREATE TABLE IF NOT EXISTS wtc.occupants (
    room_id VARCHAR(36) NOT NULL,
    user_id VARCHAR(36) NOT NULL,
    role VARCHAR(10) NOT NULL,
    PRIMARY KEY (room_id, user_id, role)
);

CREATE TABLE IF NOT EXISTS wtc.users (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(100) NOT NULL,
    nickname VARCHAR(20) NOT NULL,
    email_address VARCHAR(200) NOT NULL,
    password VARCHAR(100),
    token VARCHAR(1000),
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS user_nickname_idx ON wtc.users (nickname);

CREATE UNIQUE INDEX IF NOT EXISTS user_email_address_idx ON wtc.users (nickname);