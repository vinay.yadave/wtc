/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.mongo.service;

import com.buralo.wtc.api.domain.Message;
import com.buralo.wtc.api.mongo.domain.MessageImpl;
import com.buralo.wtc.api.service.database.AbstractMessageService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class MongoMessageService extends AbstractMessageService {

    private static final String MESSAGES_COLLECTION = "messages";
    private static final String ROOM_COLUMN = "room";
    private static final String MESSAGE_TIMESTAMP_COLUMN = "messageTimestamp";
    private final ReactiveMongoOperations mongoOperations;

    MongoMessageService(final ReactiveMongoOperations mongoOperations,
                        final IdentifierService identifierService,
                        final Clock clock) {
        super(identifierService, clock);
        this.mongoOperations = mongoOperations;
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return mongoOperations.find(createQuery(room, Long.valueOf(marker)), Message.class, MESSAGES_COLLECTION);
    }

    private Query createQuery(final String room,
                              final long timestamp) {
        return new Query()
                .addCriteria(Criteria.where(MESSAGE_TIMESTAMP_COLUMN).gt(timestamp).and(ROOM_COLUMN).is(room))
                .with(Sort.by(Sort.Direction.ASC, MESSAGE_TIMESTAMP_COLUMN));

    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new MessageImpl(id, room, sender, language, text, getClock().millis()))
                .flatMap(message -> mongoOperations.insert(message, MESSAGES_COLLECTION));
    }
}
