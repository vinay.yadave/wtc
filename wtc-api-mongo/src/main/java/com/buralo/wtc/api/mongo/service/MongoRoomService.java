/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.mongo.service;

import com.buralo.wtc.api.domain.Room;
import com.buralo.wtc.api.domain.RoomFactory;
import com.buralo.wtc.api.mongo.domain.RoomImpl;
import com.buralo.wtc.api.mongo.repository.MongoRoomRepository;
import com.buralo.wtc.api.service.database.AbstractRoomService;
import com.buralo.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MongoRoomService extends AbstractRoomService {

    private final RoomFactory roomFactory;

    private final MongoRoomRepository repository;

    MongoRoomService(final RoomFactory roomFactory,
                     final IdentifierService identifierService,
                     final MongoRoomRepository repository) {
        super(identifierService);
        this.roomFactory = roomFactory;
        this.repository = repository;
    }

    @Override
    public Flux<Room> fetchRooms() {
        return repository
                .findAll(Sort.by("name"))
                .cast(Room.class);
    }

    @Override
    public Mono<Room> createRoom(final String name,
                                 final boolean secret) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> roomFactory.createRoom(id, name, secret))
                .cast(RoomImpl.class)
                .flatMap(repository::insert);
    }

    @Override
    public Mono<Room> getRoom(final String id) {
        return repository
                .findById(id)
                .cast(Room.class);
    }

    @Override
    public Mono<Void> updateRoom(final String id,
                                 final String name,
                                 final boolean secret) {
        return repository
                .findById(id)
                .map(room -> roomFactory.createRoom(id, name, secret))
                .cast(RoomImpl.class)
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteRoom(final String id) {
        return repository
                .deleteById(id)
                .then();
    }
}
