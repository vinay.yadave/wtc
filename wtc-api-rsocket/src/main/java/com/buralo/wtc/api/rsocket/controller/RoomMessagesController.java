package com.buralo.wtc.api.rsocket.controller;

import com.buralo.wtc.api.payload.MessageResponse;
import com.buralo.wtc.api.payload.MessageRequest;
import com.buralo.wtc.api.rsocket.service.MessageService;
import com.buralo.wtc.api.service.translate.TranslationService;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class RoomMessagesController {

    private final MessageService messageService;

    RoomMessagesController(final MessageService messageService) {
        this.messageService = messageService;
    }

    @MessageMapping("users.{user}.rooms.{room}.fetch")
    public Flux<MessageResponse> fetchMessages(@DestinationVariable final String user,
                                               @DestinationVariable final String room,
                                               final Integer request) {
        return messageService
                .streamMessages(
                        room,
                        request.longValue()
                );
    }

    @MessageMapping("users.{user}.rooms.{room}.send")
    public Mono<Void> sendMessage(@DestinationVariable final String user,
                                  @DestinationVariable final String room,
                                  final MessageRequest request) {
        return messageService
                .postMessage(
                        room,
                        user,
                        request.getText(),
                        request.getLanguage()
                )
                .then();
    }

    @MessageMapping("users.{sender}.rooms.{room}.post")
    public Mono<MessageResponse> postMessage(@DestinationVariable final String sender,
                                             @DestinationVariable final String room,
                                             final MessageRequest request) {
        return messageService
                .postMessage(
                        room,
                        sender,
                        request.getText(),
                        request.getLanguage()
                );
    }

    @MessageMapping("users.{sender}.rooms.{room}.channel")
    public Flux<MessageResponse> roomChannel(@DestinationVariable final String sender,
                                             @DestinationVariable final String room,
                                             final Flux<MessageRequest> request) {
        request.map(message ->
                messageService.postMessage(
                        room,
                        sender,
                        message.getText(),
                        message.getLanguage()
                )
        ).subscribe();
        return messageService.streamMessages(
                room,
                0L
        );
    }
}