package com.buralo.wtc.api.rsocket.service;

import com.buralo.wtc.api.payload.MessageResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MessageService {

	Flux<MessageResponse> streamMessages(String room, long timestamp);
	
	Mono<MessageResponse> postMessage(String room, String sender, String text, String language);
}
