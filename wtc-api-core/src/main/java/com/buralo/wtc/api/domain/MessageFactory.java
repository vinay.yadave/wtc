package com.buralo.wtc.api.domain;

public interface MessageFactory {

    Message createMessage(
            String id,
            String room,
            String sender,
            String messageLanguage,
            String messageText,
            long messageTimestamp);
}
