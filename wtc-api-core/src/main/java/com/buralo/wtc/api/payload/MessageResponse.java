/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"id", "sender", "text", "language", "timestamp", "marker"})
public class MessageResponse {

    private final String id;

    private final String sender;

    private final String text;

    private final String language;

    private final long timestamp;

    private final String marker;

    @JsonCreator
    public MessageResponse(@JsonProperty("id") final String id,
                           @JsonProperty("sender") final String sender,
                           @JsonProperty("text") final String text,
                           @JsonProperty("language") final String language,
                           @JsonProperty("timestamp") final long timestamp,
                           @JsonProperty("marker") final String marker) {
        this.id = id;
        this.sender = sender;
        this.text = text;
        this.language = language;
        this.timestamp = timestamp;
        this.marker = marker;
    }

    public String getId() {
        return id;
    }

    public String getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }

    public String getLanguage() {
        return language;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMarker() { return marker; }
}
