/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"text", "language"})
public class MessageRequest {

    private final String text;

    private final String language;

    @JsonCreator
    public MessageRequest(@JsonProperty("text") final String text,
                          @JsonProperty("language") final String language) {
        this.text = text;
        this.language = language;
    }


    public String getText() {
        return text;
    }

    public String getLanguage() {
        return language;
    }
}
