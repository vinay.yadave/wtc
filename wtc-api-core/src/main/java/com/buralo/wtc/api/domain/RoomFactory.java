package com.buralo.wtc.api.domain;

public interface RoomFactory {

    Room createRoom(
            String id,
            String name,
            boolean secret);
}
