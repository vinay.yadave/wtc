package com.buralo.wtc.api.couchbase.repository;

import com.buralo.wtc.api.couchbase.domain.CouchbaseRoom;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseSortingRepository;
import reactor.core.publisher.Mono;

@N1qlPrimaryIndexed
public interface CouchbaseRoomRepository extends ReactiveCouchbaseSortingRepository<CouchbaseRoom, String> {

    Mono<CouchbaseRoom> findByName(String name);
}
