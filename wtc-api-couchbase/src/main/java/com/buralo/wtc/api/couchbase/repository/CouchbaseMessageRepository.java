package com.buralo.wtc.api.couchbase.repository;

import com.buralo.wtc.api.couchbase.domain.CouchbaseMessage;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseSortingRepository;
import reactor.core.publisher.Flux;

@N1qlPrimaryIndexed
public interface CouchbaseMessageRepository extends ReactiveCouchbaseSortingRepository<CouchbaseMessage, String> {

    Flux<CouchbaseMessage> findMessagesByRoomAndMessageTimestampGreaterThanOrderByMessageTimestamp(String room, long messageTimestamp);
}
