package com.buralo.wtc.api.couchbase.domain;

import com.buralo.wtc.api.domain.Message;
import org.springframework.data.annotation.Id;

public class CouchbaseMessage implements Message {

    @Id
    private final String id;

    private final String room;

    private final String sender;

    private final String messageLanguage;

    private final String messageText;

    private final long messageTimestamp;

    public CouchbaseMessage(final String id,
                            final String room,
                            final String sender,
                            final String messageLanguage,
                            final String messageText,
                            final long messageTimestamp) {
        this.id = id;
        this.room = room;
        this.sender = sender;
        this.messageLanguage = messageLanguage;
        this.messageText = messageText;
        this.messageTimestamp = messageTimestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRoom() {
        return room;
    }

    @Override
    public String getSender() {
        return sender;
    }

    @Override
    public String getMessageLanguage() {
        return messageLanguage;
    }

    @Override
    public String getMessageText() {
        return messageText;
    }

    @Override
    public long getMessageTimestamp() {
        return messageTimestamp;
    }
}
