package com.buralo.wtc.api.couchbase.repository;

import com.buralo.wtc.api.couchbase.domain.CouchbaseUser;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseSortingRepository;
import reactor.core.publisher.Mono;

@N1qlPrimaryIndexed
public interface CouchbaseUserRepository extends ReactiveCouchbaseSortingRepository<CouchbaseUser, String> {

    Mono<CouchbaseUser> findByNicknameOrEmailAddress(String nickname, String emailAddress);
}
