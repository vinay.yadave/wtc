package com.buralo.wtc.api.couchbase.domain;

import com.buralo.wtc.api.domain.Message;
import com.buralo.wtc.api.domain.MessageFactory;
import org.springframework.stereotype.Component;

@Component
public class CouchbaseMessageFactory implements MessageFactory {
    @Override
    public Message createMessage(final String id,
                                 final String room,
                                 final String sender,
                                 final String messageLanguage,
                                 final String messageText,
                                 final long messageTimestamp) {
        return new CouchbaseMessage(
                id,
                room,
                sender,
                messageLanguage,
                messageText,
                messageTimestamp);
    }
}
