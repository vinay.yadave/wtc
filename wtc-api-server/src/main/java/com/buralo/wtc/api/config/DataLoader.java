/*
 * Copyright 2019 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralo.wtc.api.config;

import com.buralo.wtc.api.service.database.RoomService;
import com.buralo.wtc.api.service.database.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuples;

@Component
public class DataLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);

    private final RoomService roomService;

    private final UserService userService;

    public DataLoader(final RoomService roomService,
                      final UserService userService) {
        this.roomService = roomService;
        this.userService = userService;
    }

    @EventListener
    @Order(100)
    public void loadData(@NonNull final ApplicationReadyEvent event) {

        // Populate with sample data

        roomService
                .createRoom("General", false)
                .onErrorContinue((ex, obj) -> LOGGER.warn("Room already exists {}", obj, ex))
                .thenMany(Flux.just(
                        Tuples.of("Brian Matthews", "bmatthews68", "brian@btmatthews.com"),
                        Tuples.of("Yaromir Popov Matthews", "yaromir05", "yaromir@btmatthews.com"),
                        Tuples.of("Elena Popova", "elena77", "elena@btmatthews.com")))
                .flatMap(t -> userService.createUser(t.getT1(), t.getT2(), t.getT3()))
                .onErrorContinue((ex, obj) -> LOGGER.warn("User already exists {}", obj, ex))
                .flatMap(user -> userService.setUserPassword(user.getId(), user.getToken(), "password"))
                .blockLast();
    }
}
